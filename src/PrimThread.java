import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Primzahl berechnen
 * Compile: javac.exe PrimThread.java
 * Execute: java PrimThread
 * @author Adriano Zuccarello
 *
 */

class PrimCalc implements Runnable {
	private PrimParameter args;
	private long primcount = 0;
	
	public PrimCalc(PrimParameter primParameter) {
		this.args = primParameter;
	}

	@Override
	public void run() {
		compute();
	}

	public PrimParameter getArgs() { return args; }
	
	private void compute() {
		int threadId = (int)args.getThreadid();
		//Alle Zahlen von Anfangszaehler bis zur Grenze ueberpruefen
		for(long i=args.getZaehler(); i <= args.getGrenze(); i++) {
			if(isPrim(i)) {
				primcount++;
				args.setPrimzahl(i);
				System.out.print(threadId);
			}
			else {
				System.out.print(".");
			}
		}
		
		args.setThreadendzeit(System.currentTimeMillis());
		args.setCountprim(primcount);
	}
	
	private boolean isPrim(long i) {
		if(i<2) return false;
		if(i==2) return true;
		if(i%2==0) return false;
		
		for(int x=3; x <= Math.sqrt(i); x+=2) {
			if(i%x==0)
				return false;
		}
		return true;
	}
}


class PrimParameter {
	private long threadid;
	private long startzeit;
	private long threadendzeit;
	private long zaehler;
	private long grenze;
	private long countprim = 0;
	private List<Long> primzahl = new ArrayList<Long>();
	
	public long getThreadid() { return threadid; } 
	public void setThreadid(long threadid) { this.threadid = threadid; }
	
	public long getStartzeit() { return startzeit; }
	public void setStartzeit(long startzeit) { this.startzeit = startzeit; }
	
	public long getZaehler() { return zaehler; }
	public void setZaehler(long zaehler) { this.zaehler = zaehler; }
	
	public long getGrenze() { return grenze; }
	public void setGrenze(long grenze) { this.grenze = grenze; }
	
	public long getCountprim() { return countprim; }
	public void setCountprim(long countprim) { this.countprim = countprim; }
	
	public long getThreadendzeit() { return threadendzeit; }
	public void setThreadendzeit(long threadendzeit) { this.threadendzeit = threadendzeit; }
	
	public List<Long> getPrimzahl() { return primzahl; }
	public void setPrimzahl(long primzahl) { this.primzahl.add(primzahl); }
}

public class PrimThread {
	
	private static final int NUM_THREAD = 2 * Runtime.getRuntime().availableProcessors();
	private static long grenze, zaehler;
	private static long kgv;

	public static void main(String[] args) throws InterruptedException {
		System.out.println("Start calculations");
		List<Thread> listThread = new ArrayList<Thread>();
		List<PrimParameter> prim_args = new ArrayList<PrimParameter>();
		List<PrimCalc> prim_compute = new ArrayList<PrimCalc>();
		
		if(args.length > 0) {
			grenze = Long.parseLong(args[0]);
		}
		else {
			// create a scanner so we can read the command-line input
		    Scanner scanner = new Scanner(System.in);
		    System.out.println("Bis zu welchem Wert sollen die Primzahlen berechnet werden?");
		    System.out.print("Ihre Eingabe: ");
		    grenze = scanner.nextLong();
		    System.out.println("\n");
		    scanner.close();
		}
		
		long startTicks = System.currentTimeMillis();
		
		if(grenze > 999) {
			kgv = Math.abs(grenze / NUM_THREAD);
			
			for(int t=0; t<NUM_THREAD; t++) {
				PrimParameter primparam = new PrimParameter();
				
				primparam.setStartzeit(startTicks);
				primparam.setZaehler(zaehler);
				primparam.setGrenze((t+1)*kgv);
				
				prim_args.add(primparam);
				
				zaehler = primparam.getGrenze()+1;
			}
			
			for(int t=0; t<NUM_THREAD; t++) {
				prim_args.get(t).setThreadid(t);
				prim_compute.add(new PrimCalc(prim_args.get(t)));
				listThread.add(new Thread(prim_compute.get(t)));
			}
		} 
		else {
			PrimParameter primparam = new PrimParameter();
			primparam.setStartzeit(startTicks);
			primparam.setZaehler(zaehler);
			primparam.setGrenze(grenze);
			
			prim_compute.add(new PrimCalc(primparam));
			listThread.add(new Thread(prim_compute.get(0)));
		}
		
		//starting all Threads
		System.out.println("\nThreads werden gestartet");
		for(Thread th : listThread) { th.start(); }
		
		//active Threads...
//		Thread [] threads = new Thread[Thread.activeCount()];
//		int n = Thread.enumerate(threads);
//		for(int i=0; i<n; i++) {
//			System.out.println(threads[i].toString());
//		}
		
		//wait for all Threads finished
		for(Thread th : listThread) { th.join(); }
		System.out.println("\nAlle Threads sind zurueck");
		
		//alle gesammelte Prim ausgeben/Threads
		int ausgabezaehler = 0;
		for(PrimCalc pc : prim_compute) {
			
			PrimParameter pp = pc.getArgs();
			System.out.printf("\n\nThreadID-%d\n", pp.getThreadid());
			
			for(long prim : pp.getPrimzahl()) {
				if(ausgabezaehler > 6) {
					System.out.println("");
					ausgabezaehler = 0;
				}
				System.out.print("\t"+ prim);
				ausgabezaehler++;
			}
		}
		
		//total ueberischt Prims
		System.out.println("\n");
		for(PrimCalc pc : prim_compute) {
			PrimParameter pp = pc.getArgs();
			System.out.printf("\nThreadID-%d\tElapsedTime [ms]: %d\tPrimRange: %d - %d\tPrimCounts: %d", 
					pp.getThreadid(), (pp.getThreadendzeit()-pp.getStartzeit()), pp.getZaehler(), pp.getGrenze(), pp.getCountprim());
		}
		
		
		long stopTicks = System.currentTimeMillis();
		System.out.println("\n\nTotal time [ms]: " + (stopTicks-startTicks));
	}

}
