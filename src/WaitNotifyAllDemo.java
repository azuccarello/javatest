/**
 * 
 */

/**
 * @author Adriano Zuccarello
 *
 */

public class WaitNotifyAllDemo {

	public static void main(String[] args) {
		Object lock = new Object();
		
		MyThread mt1 = new MyThread(lock);
		MyThread mt2 = new MyThread(lock);
		MyThread mt3 = new MyThread(lock);
		
		mt1.setName("A");
		mt2.setName("B");
		mt3.setName("C");
		
		mt1.start();
		mt2.start();
		mt3.start();
		
		System.out.println("main thread sleeping");
		try {
			Thread.sleep(3000);
		}
		catch(InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("main thread awake");
		synchronized (lock) {
			lock.notifyAll();
		}
	}
}


class MyThread extends Thread {
	private Object o;
	
	MyThread(Object o) {
		this.o = o;
	}
	
	@Override
	public void run() {
		synchronized (o) {
			try {
				System.out.println(getName() +" befor wait");
				o.wait();
				System.out.println(getName() +" after wait");
			}
			catch(InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}

// Session-Log:
//main thread sleeping
//A befor wait
//C befor wait
//B befor wait
//main thread awake
//B after wait
//C after wait
//A after wait
