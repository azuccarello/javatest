import java.util.concurrent.Semaphore;

/**
 * @author Adriano Zuccarello
 *
 */

public class SemaphoreTest {
	private Semaphore upperLimit = new Semaphore(5,true);
	private Semaphore lowerLimit = new Semaphore(0, true);
	private Semaphore mutex = new Semaphore(1,true);

	public static void main(String[] args) throws InterruptedException {
		SemaphoreTest st = new SemaphoreTest();
		
		System.out.println("mutex queue length: "+ st.mutex.availablePermits());
		st.mutex.acquire();
		System.out.println("mutex queue length: "+ st.mutex.availablePermits());
		st.mutex.release();
		System.out.println("mutex queue length: "+ st.mutex.availablePermits());
		st.mutex.release();
		System.out.println("mutex queue length: "+ st.mutex.availablePermits());
		st.mutex.acquire();
		System.out.println("mutex queue length: "+ st.mutex.availablePermits());
		
		System.out.println("lowerlimit queue length: "+ st.lowerLimit.availablePermits());
		st.lowerLimit.acquire();
		System.out.println("lowerlimit queue length: "+ st.lowerLimit.availablePermits());
		st.lowerLimit.release();
		System.out.println("lowerlimit queue length: "+ st.lowerLimit.availablePermits());
		st.lowerLimit.release();
		System.out.println("lowerlimit queue length: "+ st.lowerLimit.availablePermits());
		st.lowerLimit.release();
		System.out.println("lowerlimit queue length: "+ st.lowerLimit.availablePermits());
		st.lowerLimit.release();
		System.out.println("lowerlimit queue length: "+ st.lowerLimit.availablePermits());
		st.lowerLimit.release();
		System.out.println("lowerlimit queue length: "+ st.lowerLimit.availablePermits());
		st.lowerLimit.release();
		System.out.println("lowerlimit queue length: "+ st.lowerLimit.availablePermits());
		st.lowerLimit.release();
		System.out.println("lowerlimit queue length: "+ st.lowerLimit.availablePermits());
		st.lowerLimit.acquire();
		System.out.println("lowerlimit queue length: "+ st.lowerLimit.availablePermits());
	}

}
