/**
 * How many Threads are active
 */

/**
 * @author Adriano Zuccarello
 *
 */

public class Census {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Thread [] threads = new Thread[Thread.activeCount()];
		int n = Thread.enumerate(threads);
		for(int i=0; i<n; i++) {
			System.out.println(threads[i].toString());
		}
	}
}

// Session-Log
// Thread[main,5,main]
// left main = threads-name
// 5 = threads priority
// right main = threads-group