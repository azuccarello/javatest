/**
 * @author Adriano Zuccarello
 *
 */

public class SynchronizationDemo {

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		FinTrans ft = new FinTrans();
		
		TransThread tt1 = new TransThread(ft, "Deposit Thread");
		TransThread tt2 = new TransThread(ft, "Withdrawal Thread");
		
		tt1.start();
		tt2.start();

		tt2.join();
		tt1.join();
		System.out.println("SynchronizationDemo Program End");
	}

}

class FinTrans {
	private String transName;
	private double amount;
	
	public void update(String transName, double amount) {
		this.transName = transName;
		this.amount += amount;
		System.out.println("update: "+ this.transName +" "+ this.amount);
	}
}

class TransThread extends Thread {
	private FinTrans ft;
	
	TransThread(FinTrans ft, String name) {
		super(name); 	// Save thread's name
		this.ft = ft; 	// Save reference to financial transation object
	}
	
	@Override
	public void run() {
		for(int i=0; i<5; i++) {
			if(this.getName().equals("Deposit Thread")) {
				ft.update("Deposit", 2000.0);
			}
			else {
				ft.update("Withdrawal", -250.0);
			}
		}
	}
}

// Session-Log:
//update: Withdrawal -250.0
//update: Withdrawal 1500.0
//update: Withdrawal 1250.0
//update: Withdrawal 1000.0
//update: Withdrawal 750.0
//update: Deposit 1750.0
//update: Deposit 2750.0
//update: Deposit 4750.0
//update: Deposit 6750.0
//update: Deposit 8750.0
//SynchronizationDemo Program End
