package ObserverPattern;

import java.util.Observable;
import java.util.Observer;

/**
 * @author Adriano Zuccarello
 *
 */

class MyView implements Observer {
	private String mMyName;

	public MyView(String pMyName) {
		this.mMyName = pMyName;
	}
	
	@Override
	public void update(Observable obs, Object obj) {
		System.out.println(this.mMyName +": update("+ obs +","+ obj +");");
	}
}

class MyModel extends Observable {
	
	public void changeSomething() {
		setChanged();
		notifyObservers("Object or others......");
	}
	
	@Override
	public String toString() {
		return "MyModel-Observable.....have..<"+ this.countObservers() +">..Observer";
	}
}

public class ObserverDemo extends Object {
	
	MyView view, view1;
	MyModel model;

	public ObserverDemo() {
		view = new MyView("view0"); 
		view1 = new MyView("view1");
		
		model = new MyModel();
		
		model.addObserver(view);
		model.addObserver(view1);
	}
	
	public void demo() {
		model.changeSomething();
	}
	
	public static void main(String[] args) {
		ObserverDemo me = new ObserverDemo();
		me.demo();
	}
}

//Session-Log:
//view1: update(MyModel-Observable.....have..<2>..Observer,Object or others......);
//view0: update(MyModel-Observable.....have..<2>..Observer,Object or others......);
