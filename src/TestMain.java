import java.util.Random;
/**
 * @author Adriano Zuccarello
 *
 */

class Message {
    private String msg;
     
    public Message(String str){
        this.msg=str;
    }
 
    public synchronized String getMsg() throws InterruptedException {
    	if(msg == null || msg.isEmpty())
    		wait();
    	
    	return msg;
    }
 
    public synchronized void setMsg(String str) {
        this.msg=str;
        notifyAll();
    }
 
}

class TestThread {
	private Thread thread;
	private Message msg;
	
	public TestThread(String sMSG, int n) {
		Random random = new Random();
		random.setSeed(4711); //damit immer die gleiche Zufallszahl in der Reihenfolge liefert!
		
		msg = new Message(sMSG);

		thread = new Thread(new Runnable() {
			@Override
			public void run() {
				String sThreadsName = Thread.currentThread().getName();
				System.out.println(sThreadsName +"-random: "+ random.nextInt(n));
//				synchronized (msg) {
					System.out.println("begin Threads for wait");
					msg.setMsg(sThreadsName +" waiting to get notified at time: "+ System.currentTimeMillis());
//						msg.wait();
					System.out.println("after wait");
//				}
			}
		});
		thread.start();
	}
	
	public TestThread notify4wait() {
		String sThreadsName = Thread.currentThread().getName();
//		synchronized (msg) {
			msg.setMsg(sThreadsName +" Notifier work done");
//			msg.notify();
//			System.out.println("befor notify all");
//			msg.notifyAll();
//			System.out.println("after notifyall");
//		}
		return this;
	}
	public synchronized TestThread waitToFinish() throws InterruptedException {
		thread.join();
		return this;
	}
}

public class TestMain {
	private final static int StartBudget = 1000;
	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		
		TestThread tt;
		for(int i=0; i<10; i++) {
//			new TestThread(Integer.toString(i), StartBudget).notify4wait().waitToFinish();
			tt = new TestThread(Integer.toString(i), StartBudget);
			tt.waitToFinish();
			tt.notify4wait();
		}
		System.out.println("TestMain-End");
	}
}
